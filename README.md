# drupal-linkchecker

RPM packaging for the Drupal [Link
checker](https://www.drupal.org/project/linkchecker) module.

## Copr Respository 

If you are using Fedora, RHEL or CentOS (or similar) then you can
install the `drupal7-linkchecker` RPM via a [Copr
repository](https://copr.fedorainfracloud.org/coprs/danieljrmay/drupal-packages/).

```console
$ dnf copr enable danieljrmay/drupal-packages
$ dnf install drupal7-linkchecker
```
