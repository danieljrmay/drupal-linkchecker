%{?drupal7_find_provides_and_requires}

%global module linkchecker

Name:          drupal7-%{module}
Version:       1.4
Release:       1%{?dist}
Summary:       Check for broken links in Drupal

License:       GPLv2+
URL:           http://drupal.org/project/%{module}
Source0:       http://ftp.drupal.org/files/projects/%{module}-7.x-%{version}.tar.gz

BuildArch:     noarch
BuildRequires: drupal7-rpmbuild >= 7.23-3

Requires:      drupal7


%description
The linkchecker module extracts links from your content when saved and
periodically tries to detect broken hypertext links by checking the
remote sites and evaluating the HTTP response codes. It shows all
broken links in the reports/logs section and on the content edit page,
if a link check has been failed. An author specific broken links
report is also available in My Account.

This package provides the following Drupal module:
* %{module}


%prep
%setup -qn %{module}


%build
# Empty build section, nothing to build


%install
mkdir -p %{buildroot}%{drupal7_modules}/%{module}
cp -pr * %{buildroot}%{drupal7_modules}/%{module}/


%files
%license LICENSE.txt
%doc README.txt CHANGELOG.txt
%{drupal7_modules}/%{module}
%exclude %{drupal7_modules}/%{module}/*.txt


%changelog
* Tue Nov 27 2018 Daniel J. R. May <daniel.may@kada-media.com> - 1.4-1
- Initial release of RPM packaging.


